#include "csapp.h"

int main(int argc, char **argv)
{
	FILE* fichero;
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	rio_t rio;
	char respuesta[10];
	char *filename;
	char tam[20];
	if (argc != 4) {
		fprintf(stderr, "usage: %s <host> <port> <filename>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];
	filename = argv[3];
	char nombre[strlen(filename)+1];


	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);
	strcat(filename,"\n");

	Rio_writen(clientfd, filename, strlen(filename));//envia el nombre del archivo
	
	while((Rio_readlineb(&rio, buf, MAXLINE)!=0)){
		//printf("tamaño: %s\n", buf );
		//Fputs(buf, stdout);
		strcpy(tam,buf);

		break;
	}//recibe el tamaño
	
	if (strcmp(tam,"-1\n")==0)
	{
		strcpy(respuesta,"NOT\n");
		printf("No se encontró el archivo\n");
		Close(clientfd);
		return -1;
	}
	else
	strcpy(respuesta,"OK\n");
	Rio_writen(clientfd, respuesta, strlen(respuesta));//envia la respuesta
	
	
	
	strcpy(nombre,filename);
	strcat(nombre,"1");
    fichero = fopen(nombre, "wt");
	while((Read(clientfd, buf, MAXLINE)!=0)){ //recibe el archivo
		fputs(buf, fichero);
		break;
	}
	printf("Archivo Recibido\n");
	fclose(fichero); 

	Close(clientfd);
	exit(0);
}

 

	
			

