#include "csapp.h"
#include <string.h>
#include <ctype.h>

void echo(int connfd);
void trim(char *);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		echo(connfd);
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	char respuesta[MAXLINE];
	size_t n;
	char buf[MAXLINE];
	rio_t rio;
	char archivo[MAXLINE];
	struct stat sbuf;
	char *srcp;
	int srcfd;
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		if(strcmp(buf,"OK\n")==0){
			printf("archivo a enviar: %s\n", archivo);
			srcfd = Open(archivo, O_RDONLY, 0);
			srcp = Mmap(0, sbuf.st_size, PROT_READ, MAP_PRIVATE, srcfd, 0);
			Close(srcfd);
			Rio_writen(connfd, srcp, sbuf.st_size);
			printf("Archivo enviado\n");
			Munmap(srcp, sbuf.st_size);		
		}else{
			if(strcmp(buf,"NOT\n")==0){
				strcpy(respuesta,"Error 404\n");
				//Rio_writen(connfd, respuesta, strlen(respuesta));
			}else{
				trim(buf);
				if (stat(buf, &sbuf) < 0) {
					strcpy(respuesta,"-1\n");
				}
			else{
				sprintf(respuesta,"%ld\n",sbuf.st_size);
				strcpy(archivo,buf);
				}
				Rio_writen(connfd, respuesta, strlen(respuesta));
		}

	}
}
}

void trim(char *str)
{
    char *start, *end;

    /* Find first non-whitespace */
    for (start = str; *start; start++)
    {
        if (!isspace((unsigned char)start[0]))
            break;
    }

    /* Find start of last all-whitespace */
    for (end = start + strlen(start); end > start + 1; end--)
    {
        if (!isspace((unsigned char)end[-1]))
            break;
    }

    *end = 0; /* Truncate last whitespace */

    /* Shift from "start" to the beginning of the string */
    if (start > str)
        memmove(str, start, (end - start) + 1);
}